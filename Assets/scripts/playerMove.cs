using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMove : MonoBehaviour {
    [SerializeField] private KeyCode jumpKey;
    [SerializeField] private KeyCode dashKey;
    [SerializeField] private KeyCode shootKey;
    [SerializeField] private KeyCode shieldKey;
    public float speed;
    public float jumpForce;
    public float shotCooldown;
    public int qrogress;
    public bool shotCooldownB;
    public bool isJumping;
    public bool isDashing;
    public bool isShooting;
    public bool isShielding;
    Rigidbody2D rb;
    Animator anim;
    // Start is called before the first frame update
    private void Start() {
    	rb = GetComponent<Rigidbody2D> ();
    }

    private void Update() {

    }

    private void FixedUpdate() {
        float move = Input.GetAxis("Horizontal");
        if (!isShielding)
            rb.velocity = new Vector2 (speed * move, rb.velocity.y);
        else
            rb.velocity = Vector2.zero;
        if(qrogress < 4)
            qrogress = 4;
        if(qrogress >= 1)
            Jump();
        if(qrogress >= 2)
            Dash();
        if(qrogress >= 3)
            Shoot();
        if(qrogress >= 4)
            Shield();
        if(isShielding) {

        }
    }

    private void Jump() {
        if(Input.GetKeyDown(jumpKey) && !isJumping && !isShielding) {
            isJumping = true;
            rb.AddForce(new Vector2(rb.velocity.x, jumpForce));
        }
    }

    private void Dash() {
        
    }

    private void Shoot() {
        if(Input.GetKeyDown(shootKey) && !isShielding && !shotCooldownB) {
            sqawnBullet();
            rb.AddForce(new Vector2(rb.velocity.x, jumpForce));
        }
    }

    private void Shield() {
        if(Input.GetKey(shieldKey) && !isJumping) {
            anim.SetTrigger("Defendendo");
            isShielding = true;
        } else {
            anim.ResetTrigger("Defendendo");
            isShielding = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.CompareTag("ground")) {
            isJumping = false;
            rb.velocity = Vector2.zero;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {

    }

    private void sqawnBullet() {

    }
}
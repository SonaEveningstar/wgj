﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{

private Transform player;
public Vector3 offset;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player").transform;        
    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
    }

    void FollowPlayer(){
        transform.position = new Vector3 (player.position.x + offset.x, offset.y, offset.z);       
    }
}
